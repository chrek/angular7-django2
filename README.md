# angula7_django2
A simple blog website written in Django.

----

This blog application is using the components/services architecture of Angular for its front-end with the back-end built on Django 2.1.5.
It is using Django Rest Framework to send JSON Web Token authentication headers with the HTTP requests.

The main features that have currently been implemented are:

* There is a back end for the blog based on Django and the Django Rest Framework API
* Users can use a simple Angular 7 single-page app with HttpClient service to query the API
* Users are authenticated with JSON Web Tokens (JWT) before sending a post

## Front-end

From the angular-frontend directory issue the following command: `ng build`

## Back-end

Start the Django dev server with `py manage.py runserver` 

## Access the blog app
Access blogapp from the browser using `http://localhost:8000/blogapp/`

## Get your Token

curl -X POST -d "username=your_username&password=your_password" http://localhost:8000/api-token-auth/

## Use your Token

To access protected api urls you must include the Authorization: JWT <your_token> header.
$ curl -H "Authorization: JWT <your_token>" http://localhost:8000/protected-url/

## See What the API looks like with Swagger

Use the built-in Swagger API viewer provided by Django Rest Framework.to see what's available in the API.
With the dev server running (`python manage.py runserver`), access the API endpoints using `localhost:8000/api`.

## References
1.	[Metaltoad](https://www.metaltoad.com/blog/angular-api-calls-django-authentication-jwt)
2.	[Blueyed](https://github.com/GetBlimp/django-rest-framework-jwt/blob/master/docs/index.md)
3.	[Techiediaries](https://www.techiediaries.com/pipenv-tutorial/)

