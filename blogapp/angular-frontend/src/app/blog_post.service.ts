import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {UserService} from './user.service';
 
@Injectable()
export class BlogPostService {
 
  constructor(private http: HttpClient, private _userService: UserService) {
  }

  // Uses http.get() to load data from a single API endpoint
  list() {
    //return this.http.get('api/posts');
    return this.http.get('/blogapp/api/posts');
  }
 
  // send a POST request to the API to create a new blog post
  create(post, token) {
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'JWT ' + this._userService.token // this is our token from the UserService
      })
    };
    //return this.http.post('/api/posts', JSON.stringify(post), httpOptions);
    return this.http.post('/blogapp/api/posts', JSON.stringify(post), httpOptions);
  }
 
}
